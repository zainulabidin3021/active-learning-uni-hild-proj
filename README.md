# Active Learning Augmentation Image Classification Project

## How to contribute?

1. Create an issue under a milestone with the following naming convention MSXXISXX - Name of the issue (MSXX refers to the number of the milestone and ISXX refers to the number of the issue). Replacing the XX with the milestone number and the issue number
   ![Alt text](./resources/issue-naming.png?raw=true "Title")
2. Create branch with the code of the issue (MSXXISXX).
3. When the code is finished, create a merge request with the title equal to the issue
4. The merge request has to be approved by 2 members of the team other than the person that created the merge request, for it to be succesfully merged into master.
